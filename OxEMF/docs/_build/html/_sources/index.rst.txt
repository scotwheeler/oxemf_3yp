.. OxEMF_3YP documentation master file, created by
   sphinx-quickstart on Wed Feb 27 11:31:33 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to OxEMF_3YP's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   API



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
