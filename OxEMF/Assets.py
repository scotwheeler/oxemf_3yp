#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
3YP basic asset module.
Adapted from UoO EPG's energy management framework.
Authors: Avinash Vijay, Scot Wheeler
"""

__version__ = '0.4'

import pandas as pd
import numpy as np
from datetime import timedelta


class Non_Dispatchable:
    """Non-dispatchable asset base class"""
    def __init__(self):
        self.dispatch_type = "Non-dispatchable"
        self.capacity = 0
        self.install_cost = 0
        self.lifetime = 20
        self.genFiT = 0


class Dispatchable:
    """Dispatchable asset base class"""
    def __init__(self):
        self.dispatch_type = "Dispatchable"
        self.capacity = 0
        self.install_cost = 0
        self.lifetime = 20
        self.genFiT = 0

# %% Non-dispatchable assets



class pvAsset(Non_Dispatchable):
    """
    PV asset class

    Parameters
    ----------
    profile : Series or Dataframe, or str
            kW/kWp profile as a pandas series or a filepath for the profile
    
    Capacity : float
        PV capacity, kW.

    install_cost : float
        £ price per kWp to install
        
    maintenance_cost : float
        Annual maintenance cost in £s
    """
    def __init__(self, capacity, profile='data/oxon_solar_2014.csv',
                 install_cost=1500,
                 maintenance=0, genFiT=0.05, **kwargs):
        super().__init__()
        self.profile = profile
        self.capacity = capacity
        self.asset_type = 'PV'
        self.install_cost = install_cost * 100  # p/kWp
        self.maintenance = maintenance * 100 # p per year
        self.genFiT =genFiT  # p/kWh
        if isinstance(profile, str):
            # a filepath given
            self.cf = self.loadProfile()
        else:
            self.cf = self.profile
        
    def loadProfile(self):
        """
        Loads the kW/kWp hourly solar profile


        Returns
        -------
        kW/kWp solar profile

        """
        df = pd.read_csv(self.profile, index_col=0,
                         parse_dates=True, dayfirst=True)  # kW/kWp
        dfHH = df.resample('0.5H').mean()
        # adding a missing point at the end
        dfHH = dfHH.append(pd.DataFrame({dfHH.columns[0]: np.nan},
                                      index=[(dfHH.index[-1] +
                                              timedelta(minutes=30))]))
        dfHH = dfHH.interpolate()
        
        return dfHH
        

    def getOutput(self, dt):
        """
        Return PV output

        Parameters
        ----------
        dt : float
            Time interval (hours)

        Returns
        -------
        PV output : numpy array
        """
        
        
        output = self.cf.values * self.capacity * dt  # kWh
        self.output = output
        return output


class loadAsset(Non_Dispatchable):
    """
    Load asset class

    Parameters
    ----------
    nHouses : int
        Number of houses
        
    profile_filepath : str
        Filepath to load profile
    """
    def __init__(self, nHouses, profile='data/oxon_class1_year_load.csv'):
        super().__init__()
        self.nHouses = nHouses
        self.asset_type = 'DOMESTIC_LOAD'
        self.install_cost = 0
        if isinstance(profile, str):
            self.profile = self.loadProfile()
        else:
            self.profile = profile
        
    def loadProfile(self):
        df = pd.read_csv(self.profile_filepath, usecols=[1])
        return df
        

    def getOutput(self, dt):
        """
        Return domestic demand

        Parameters
        ----------
        dt : float
            Time interval (hours)

        Returns
        -------
        Domestic demand : numpy array
        """
        dem = self.profile.values
        output = dem * self.nHouses * dt
        self.output = output
        return output

# %% Dispatchable Assets


class IdealBatteryAsset(Dispatchable):
    """
    Ideal battery asset class

    Parameters
    ----------
    Capacity : float
        Battery capacity, kWh.

    power : float
        Maximum power, kW.

    dt : float
        Time interval (hours)

    T : int
        Number of intervales
    """
    def __init__(self, capacity, power, dt, T):
        super().__init__()
        self.asset_type = 'IDEAL BATTERY'
        self.capacity = capacity
        self.power = power * dt
        self.soc = np.ones(T) * self.capacity

    def getOutput(self, net_load):
        """
        Battery control of charging/discharging in response to net load.

        Parameters
        ----------
        net_load : numpy array
            The net load, (load - nondispatchable gen).

        Returns
        -------
        Battery energy use profile : numpy array
        """
        T = len(net_load)
        output = np.zeros((T, 1))
        for j in range(len(net_load)):
            if j == 0:
                soc = self.capacity
            else:
                soc = self.soc[j-1]

            if net_load[j] > 0:  # use battery
                output[j] = min(self.power, net_load[j], soc)
                self.soc[j] = soc - output[j]
            elif net_load[j] < 0:  # charge battery
                output[j] = max(-self.power, net_load[j],
                                - (self.capacity - soc))
                self.soc[j] = soc - output[j]
            elif net_load[j] == 0:  # do nothing
                self.soc[j] = soc
        self.output = output
        return output


class PracticalBatteryAsset(Dispatchable):
    """
    Practical battery asset class

    Parameters
    ----------
    capacity : float
        Battery capacity, kWh.

    power : float
        Maximum power, kW.

    eff : float
        Charging/discharging efficiency between 0-1.
    
    self_dis : float
        The hourly self discharge fraction. 
        Default value is equivalent to 5% per month.

    dt : float
        Time interval (hours)

    T : int
        Number of intervales
        
    install_cost : float
        Install cost in £/kWh
    """
    def __init__(self, capacity, power, dt, T, eff=0.9, self_dis = (0.95**(1/720)), install_cost=500):
        super().__init__()
        self.asset_type = 'PRACTICAL BATTERY'
        self.capacity = capacity
        self.power = power * dt
        self.eff = eff
        self.self_dis = self_dis
        self.soc = np.ones(T) * self.capacity
        self.install_cost = install_cost * 100  # p/kWh
        self.dt = dt

    def getOutput(self, net_load):
        """
        Battery control of charging/discharging in response to net load.

        Parameters
        ----------
        net_load : numpy array
            The net load, (load - nondispatchable gen).

        Returns
        -------
        Battery energy use profile : numpy array
        """

        T = len(net_load)
        output = np.zeros((T, 1))
        for j in range(len(net_load)):
            if j == 0:
                soc = self.capacity
            else:
                soc = self.soc[j-1] * (self.self_dis**(self.dt))

            if net_load[j] > 0:  # use battery
                output[j] = min(self.power, net_load[j], self.eff*soc)
                self.soc[j] = soc - (1/self.eff)*output[j]
            elif net_load[j] < 0:  # charge battery
                output[j] = max(-self.power, net_load[j],
                                - (1/self.eff) * (self.capacity - soc))
                self.soc[j] = soc - self.eff * output[j]
            elif net_load[j] == 0:  # do nothing
                self.soc[j] = soc
        self.output = output
        return output


if __name__ == "__main__":
    load_site1 = loadAsset(1)
    pv = pvAsset(1, 10, 0).getOutput(0.5)
