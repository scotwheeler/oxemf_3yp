# OxEMF - Energy Management Framework

This package has been developed for teaching within the Local Energy System for
 Oxfordshire 3YP project course in Engineering Science at the
 University of Oxford.

A good place to start is the Energy Modelling tutorial in the notebooks folder.

Documentation for OxEMF_3YP can be found
[here](https://oxemf-3yp.readthedocs.io/).

This is a basic version of EPG's OPEN package which can be found on GitHub
[here](https://github.com/EPGOxford/OPEN).

Authors: Scot Wheeler, Avinash Vijay, Masaō Ashtine.
