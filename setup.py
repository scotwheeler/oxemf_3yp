from setuptools import setup, find_packages

setup(
    name="OxEMF_3YP",
    version="0.4.0",
    packages=find_packages(),

    # metadata to display on PyPI
    author="Scot Wheeler, Avinash Vijay",
    author_email="scot.wheeler@eng.ox.ac.uk",
    description="Basic energy balance for Oxford Engineering 3YP group projects",
    license="MIT",
    install_requires=['numpy',
                      'matplotlib',
                      'pandas']


)
